import React, { Suspense } from "react";
import Test from './test';
import { utils } from '../../utils';
//export const ProfileLoadable = () => import(/* webpackChunkName: "profile" */ './profile');

const OtherComponent = React.lazy(() => import(/* webpackChunkName: "aditya" */ /* webpackMode: "lazy" */
/* webpackExports: ["default", "named"] */'./profile'))
export const ProfileLoadable = () => {
    const loading = <div>Loading...</div>
    return (
        <Suspense fallback={loading}>
            <OtherComponent/>
        </Suspense>
    )
}
