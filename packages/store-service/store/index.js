import createStore from 'redux';
import storeConfig from './initial_store_config';
import reducers from '../reducers';

const store = createStore(reducers, storeConfig);

export default store;