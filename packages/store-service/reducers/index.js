import { combineReducers } from 'redux';
import profileReducer from './profile';

const reducers = combineReducers(profileReducer)

export default reducers;