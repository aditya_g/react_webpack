import React from 'react';
import { ProfileLoadable } from '@aditya_g/profile';
import {default as Dummy} from './dummy';
import { BrowserRouter  as Router,Routes, Route, Link} from 'react-router-dom';

export const RouteApp = () => {
return(
    <Router>
        <Link to='/'>Home</Link>
        <Link to='/profile'>Profile</Link>
        <Routes>
            <Route path={'/profile'} element={<ProfileLoadable />}></Route>
        </Routes>
    </Router>
)
}