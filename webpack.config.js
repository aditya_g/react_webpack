const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
    mode: 'development',
    //devtool: "source-map",
    //target: "node",
    entry: {
        app: './packages/app/index.js',
        //profile: './packages/profile/index.js'
    },
    resolve: {
        alias: {
          components: path.resolve(__dirname, 'packages'),
        },
        extensions: ['.js', '.jsx'],
      },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist'),
        libraryExport: 'default'
        //libraryTarget: 'commonjs2'
        //publicPath:'/dist'
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'dist'),
          },
          port: 5000,
          open: true,
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                // use: {
                //     loader: 'babel-loader',
                //     // options: {
                //     //   presets: ['react']
                //     // }
                //   }
                loader: 'babel-loader',
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: 'My App',
            template: './packages/app/index.html',
            filename: './index.html'
        })
    ],
    optimization: {
        splitChunks: {
            cacheGroups: {
            vendor : {
                name: "vendorbhai1",
                test: /[\\/]node_modules[\\/]/,
                chunks: "all",
            },
            common: {
                name: "utilChunk",
                test: /[\\/]utils[\\/]/,
                chunks: "all",
                minSize: 0,
            }
        }
    },
    runtimeChunk: {
             name: "manifest",
      }, 
},
externals: {
    react: "React",
    "react-dom": "ReactDOM",
    lodash: {
        commonjs: 'lodash',
        amd: 'lodash',
        root: '_', // indicates global variable
    },
},
//  performance: {
//         hints: "error",
//         maxAssetSize: 100 * 1024, // 100 KiB 
//         maxEntrypointSize: 100 * 1024, // 100 KiB
//       },
}